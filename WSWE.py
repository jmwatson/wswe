from flask import Flask
import random
import json

app = Flask(__name__)


@app.route('/')
def select_restaurant():
    restaurants = load_restaurants()
    selection = random.choice(make_weighted_list(restaurants))
    restaurants[selection] += 1
    save_restaurants(restaurants)

    return selection


@app.route('/add/<restaurant>')
def add_restaurant(restaurant):
    restaurants = load_restaurants()

    try:
        restaurants[restaurant] += 1
    except Exception:
        restaurants[restaurant] = 1

    save_restaurants(restaurants)

    return restaurant + ' added!'


def load_restaurants():
    with open('restaurants.json') as json_file:
        restaurants = json.load(json_file)
        json_file.close()

    return restaurants


def save_restaurants(restaurants):
    with open('restaurants.json', 'w') as json_file:
        json.dump(restaurants, json_file)
        json_file.close()


def make_weighted_list(restaurants):
    weighted_list = []

    for key, value in restaurants.iteritems():
        weighted_list.extend([key] * value)

    return weighted_list


if __name__ == '__main__':
    app.run()
